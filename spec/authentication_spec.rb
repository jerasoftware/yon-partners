require 'spec_helper'

describe 'Authentication' do 
  it 'when logged in' do
  	auth = YonPartners::Authentication.new("nicolas@jera.com.br", "secret123")
    expect(auth.user.code).to be_within(200).of(299)
  end
end
