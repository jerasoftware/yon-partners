require "yon_partners/version"
require 'httparty'

module YonPartners

	class Base
	  include HTTParty
	  base_uri 'staging.yonbike.jera.com.br/partners'
	end
	
end
