class YonPartners::Authentication < YonPartners::Base

	def initialize(email, password)
		options = {
			body: {
				email: email,
				password: password
			}
		}
    @auth = self.class.post('/authenticate', options)
  end

  def user
  	@auth
  end

end
